#!/bin/bash 
# QLogic FC HBA Snapshot Utility
# Copyright (C) 2006 QLogic Corporation (www.qlogic.com)
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#--------------------------------------------------------------------------#
#This utility works on the sysfs to display the information of the QLogic 
#HBA attached in the system.  
#--------------------------------------------------------------------------#

QL_ATTR_VALUE="Unknown" #Global for storing value
QL_HOST=()
QL_ISCSI_HOST=()
QL_TARGETS=()
QL_LUNS=()
QL_PRINT_LIST=0
QL_PRINT_ALL_DETAILS=0
QL_TABS=0
QL_VERSION=1.16
QL_PROC=1
QL_SYS=2
QL_FS=$QL_SYS
QL_MODULE=qla2xxx
QL_PROC_PATH=/proc/scsi/$QL_MODULE
QL_TEMP=/tmp/temp.scsi 

INIT_SPACE=0
VERBOSE=0
DEVICE_TYPES=(disk    tape    printer process worm    cd\/dvd   scanner optical mediumx comms   \(0xa\)   \(0xb\)   storage enclosu "sim dsk" "opti rd" bridge osd adi \(0x13\) \(0x14\) \(0x15\) \(0x16\) \(0x17\) \(0x18\) \(0x19\) \(0x1a\) \(0x1b\) \(0x1c\) \(0x1e\) \(0x18\) \(0x19\) \(0x1a\) \(0x1b\) \(0x1c\) \(0x1e\) wlun "no dev")
QL_SCRIPT=$0
# --------------------------------------------------------- #
# echo_b()		                                    #
# Prints messages in bold				    #
# Parameter: 						    #
#	$1	Message to be printed			    #
# Returns: None.		 			    #
# --------------------------------------------------------- #
function echo_b() {
	echo -en "\033[1m${1}\033[0m"
	tput sgr0
}

function qlu_sys_find_iscsi_host()
{
        ls -d /sys/bus/pci/drivers/qla4*/*/host*/ >& /dev/null
        if [ $? -eq 0 ]; then
               QL_ISCSI_HOST=( `ls -d /sys/bus/pci/drivers/qla4*/*/host* | sed -e "s/.*host//"` )
        else
                echo_b "QLogic iSCSI HBA not found or sysfs interface not supported"
                echo ""
		return 1
        fi
        return ${#QL_ISCSI_HOST[@]}
}

# --------------------------------------------------------- #
# qlu_sys_find_all_host()                                   #
# Look into the /sys/bus/pci/drivers/qla*/ dir to find all  #
# HBAs in the system                                        #
# Parameter:                                                #
#       None                                                #
# Returns:                                                  #
#       Fill up the QL_HOST array, returns number of hosts  #
#       found                                               #
# --------------------------------------------------------- #
	
function qlu_sys_find_all_host()
{
	ls -d /sys/bus/pci/drivers/qla2*/*/host*/ >& /dev/null 
	if [ $? -eq 0 ]; then
####	if [ -e "ls -d /sys/bus/pci/drivers/qla*/*/host*/" ]; then
	       QL_HOST=( `ls -d /sys/bus/pci/drivers/qla2*/*/host* | sed -e "s/.*host//"` )
	else
		ls -d /sys/bus/pci/drivers/qla6*/*/host*/ >& /dev/null 
		if [ $? -eq 0 ]; then

			QL_HOST=( `ls -d /sys/bus/pci/drivers/qla6*/*/host* | sed -e "s/.*host//"` )
        		return ${#QL_HOST[@]}
		fi
		echo_b "QLogic FC/FCoe HBA not found or sysfs interface not supported"
		echo ""
		return 1
	fi
        return ${#QL_HOST[@]}
}

# --------------------------------------------------------- #
# qlu_proc_find_all_host()                                  #
# Look into the /proc/scsi/qla2xxx dir to find all          #
# HBAs in the system                                        #
# Parameter:                                                #
#       None                                                #
# Returns:                                                  #
#       Fill up the QL_HOST array, returns number of hosts  #
#       found                                               #
# --------------------------------------------------------- #
function qlu_proc_find_all_host()
{
	QL_HOST=( `ls` )
	return ${#QL_HOST[@]}
}

# --------------------------------------------------------- #
# get_value ()                                              #
# gets the path as input and returns the value of the       #
# directed file if not exist returns unknown                #
# Parameters :                                              #
#		sysfs path                                  #
# Returns :                                                 #
# 		value in the file                           #
# --------------------------------------------------------- #
function get_value()
{
	QL_ATTR_VALUE="Unknown"
	if [ $# -eq 0 ]; then
		echo "Please provide sysfs path"
		return 1
	fi

	if [ -e $1 ]; then
		QL_ATTR_VALUE=`cat $1`
	fi
}

# --------------------------------------------------------- #
# print_value ()                                            #
# Takes 3 i/ps <space>  <name>  and <value> and prints      #
# <space>name = value                                       #
# Parameters :                                              #
#	<space>  <name>  and <value>                        #
# Returns :                                                 #
# 		NONE                                        #
# --------------------------------------------------------- #
function print_value()
{
	#check all parameters supplied
	if [ $# -lt 3 ]; then
		echo "Usage: print_value <Space> <name> <Value>"
	fi
	
	#print spaces
	IN_SPACE=$1
	while [ $IN_SPACE -gt 0 ]
	do
		echo -n " "
		(( IN_SPACE -= 1 ))
	done
	
	#Print Name
	echo -en "$2"
	IN_TAB=$QL_TABS
	while [ $IN_TAB -gt 0 ]
	do
		echo -en "\t"
		(( IN_TAB -= 1 ))
	done

	echo -en ": "
	#Print Value
	echo "$3"
}

# --------------------------------------------------------- #
# get_targets ()                                            #
# Detects all the targets connected to given host and fills #
# QL_TARGETS array                                          #
# Parameters :                                              #
#              HOST                                         #
# Returns :                                                 #
#           Number of targets                               #
# --------------------------------------------------------- #
function get_targets
{
local HOST=$1
local TARGET
local TARGETS=()
QL_TARGETS=()
####	if [ -e /sys/class/scsi_host/host$HOST/device/ ]; then
	if [ -e /sys/class/scsi_device ]; then
####		cd /sys/class/scsi_host/host$HOST/device/
		cd /sys/class/scsi_device/
		
####		ls -d rport-$HOST:0-* &> /dev/null
####		if [ $? -eq 0 ]; then
####			QL_TARGETS=( `ls -d rport-$HOST:0-* | sed "s/rport-$HOST:0-//"` )
####		fi

		ls -d $HOST:0:* &> /dev/null
		if [ $? -eq 0 ]; then
			TARGETS=( `ls -d $HOST:0:* | sed "s/$HOST:0:\(.*\):.*/\1/"` )
			for TARGET in ${TARGETS[@]}
			do
				echo "${QL_TARGETS[@]}" | grep -w $TARGET >& /dev/null
				if [ $? -ne 0 ]; then
					QL_TARGETS=(${QL_TARGETS[@]} $TARGET)
				fi
			done
		fi

####		ls -d target$HOST:0:* &> /dev/null
####		if [ $? -eq 0 ]; then
####			QL_TARGETS=( `ls -d target$HOST:0:* | sed "s/target$HOST:0://"` )
####		fi
	fi

	cd /sys/class/scsi_host/host$HOST/device/

	return ${#QL_TARGETS[@]}

}


# --------------------------------------------------------- #
# get_iscsi_targets ()                                            #
# Detects all the targets connected to given host and fills #
# QL_TARGETS array                                          #
# Parameters :                                              #
#              HOST                                         #
# Returns :                                                 #
#           Number of targets                               #
# --------------------------------------------------------- #
function get_iscsi_targets
{
local ISCSI_HOST=$1
local TARGET
local TARGETS=()
QL_ISCSI_TARGETS=()
        if [ -e /sys/class/scsi_device ]; then
                cd /sys/class/scsi_device/

                ls -d $ISCSI_HOST:0:* &> /dev/null
                if [ $? -eq 0 ]; then
                        TARGETS=( `ls -d $ISCSI_HOST:0:* | sed "s/$ISCSI_HOST:0:\(.*\):.*/\1/"` )
                        for TARGET in ${TARGETS[@]}
                        do
                                echo "${QL_ISCSI_TARGETS[@]}" | grep -w $TARGET >& /dev/null
                                if [ $? -ne 0 ]; then
                                	QL_ISCSI_TARGETS=(${QL_ISCSI_TARGETS[@]} $TARGET)
                                fi
                        done
                fi
        fi

	cd /sys/class/scsi_host/host$ISCSI_HOST/device/
	return ${#QL_ISCSI_TARGETS[@]}
}

# --------------------------------------------------------- #
# get_proc_targets ()                                       #
# Detects all the targets connected to given host and fills #
# QL_TARGETS array. This works proc based                   #
# Parameters :                                              #
#              HOST                                         #
# Returns :                                                 #
#           Number of targets                               #
# --------------------------------------------------------- #

function get_proc_targets
{
local HOST=$1
local TARGET=""
QL_TARGETS=()
	TARGETS=`cat $HOST | grep "^(.*:" | grep -v "Id:Lun" | sed "s/(\(.*\):.*).*/\1/"`
	for TARGET in $TARGETS
	do
		echo "${QL_TARGETS[@]}" | grep -w $TARGET >& /dev/null
		if [ $? -ne 0 ]; then
			QL_TARGETS=(${QL_TARGETS[@]} $TARGET)
		fi
	done
return ${#QL_TARGETS[@]}

}


# --------------------------------------------------------- #
# get_proc_luns ()                                          #
# Detects all the LUNS connected to given TARGET and fills  #
# QL_LUNS array, this works on proc based scanning.         #
# Parameters :                                              #
#              <HOST> <TARGET>                              #
# Returns :                                                 #
#           Number of LUNS                                  #
# --------------------------------------------------------- #

function get_proc_luns
{
local HOST=$1
local TARGET=$2
QL_LUNS=()
LUNS=`cat $HOST | sed "s/ //g" | grep "^(${TARGET}:" | sed "s/(${TARGET}:\(.*\)).*/\1/"`
for LUN in $LUNS
do
	echo "${QL_LUNS}" | grep -w $LUN >& /dev/null
	if [ $? -ne 0 ]; then
		QL_LUNS=(${QL_LUNS[@]} $LUN)
	fi
done
return ${#QL_LUNS[@]}

}

# --------------------------------------------------------- #
# get_luns ()                                               #
# Detects all the LUNS connected to given TARGET and fills  #
# QL_LUNS array                                             #
# Parameters :                                              #
#              <HOST> <TARGET>                              #
# Returns :                                                 #
#           Number of LUNS                                  #
# --------------------------------------------------------- #
function get_luns 
{
	local HOST=$1
	local TARGET=$2

####	if [ -e /sys/class/scsi_host/host$HOST/device/target${HOST}:0:${TARGET} ]; then
	if [ -e /sys/class/scsi_device/ ]; then
		cd /sys/class/scsi_device/
####		cd /sys/class/scsi_host/host$HOST/device/target${HOST}:0:${TARGET}/
####	elif [ -e /sys/class/scsi_host/host$HOST/device/rport-$HOST:0-$TARGET/target${HOST}:0:${TARGET} ]; then
####		cd /sys/class/scsi_host/host$HOST/device/rport-$HOST:0-$TARGET/target${HOST}:0:${TARGET}/
	fi

	QL_LUNS=( `ls -d $HOST:0:$TARGET:* 2> /dev/null | sed "s/$HOST:0:$TARGET://"` ) 
	return ${#QL_LUNS[@]}
	
}


# --------------------------------------------------------- #
# get_iscsi_luns ()                                               #
# Detects all the LUNS connected to given TARGET and fills  #
# QL_LUNS array                                             #
# Parameters :                                              #
#              <HOST> <TARGET>                              #
# Returns :                                                 #
#           Number of LUNS                                  #
# --------------------------------------------------------- #
function get_iscsi_luns
{
        local HOST=$1
        local TARGET=$2

        if [ -e /sys/class/scsi_device/ ]; then
                cd /sys/class/scsi_device/
        fi

        QL_ISCSI_LUNS=( `ls -d $HOST:0:$TARGET:* 2> /dev/null | sed "s/$HOST:0:$TARGET://"` )
        return ${#QL_ISCSI_LUNS[@]}
}



# --------------------------------------------------------- #
# common_display ()                                         #
# Displays common parameters for HBA's                      #
# After version 1.4, Modified for proc too                  #
# Parameters :                                              #
#              NONE                                         #
# --------------------------------------------------------- #
function common_iscsi_display
{
	if [ $QL_FS == $QL_SYS ]; then
		if [ -e /sys/class/iscsi_host/host$ISCSI_HOST ]; then
			cd /sys/class/iscsi_host/host$ISCSI_HOST
		fi
	else
		if [ -e ${QL_PROC_PATH}/$ISCSI_HOST ]; then
			cd ${QL_PROC_PATH}
		fi
	fi
	if [ $QL_FS == $QL_SYS ]; then
		get_value "initiatorname"
	else
		echo ""
	fi
	print_value $INIT_SPACE "initiatorname" "$QL_ATTR_VALUE"
	if [ $QL_FS == $QL_SYS ]; then
		get_value "ipaddress"
	else
		echo ""
	fi
	print_value $INIT_SPACE "ip address" "$QL_ATTR_VALUE"
	if [ $QL_FS == $QL_SYS ] && [ -e /sys/module/qla4xxx/ ]; then
			cd /sys/module/qla4xxx/
	fi
	if [ $QL_FS == $QL_SYS ]; then
		get_value "version"
	else
		echo ""
	fi
	print_value $INIT_SPACE "Driver Version" "$QL_ATTR_VALUE"

if [ "1" == "0" ]; then

	if [ $QL_FS == $QL_SYS ]; then
		get_value "model_name"
	else
		echo ""
	fi
        print_value $INIT_SPACE "Model" "$QL_ATTR_VALUE"
	if [ $QL_FS == $QL_SYS ]; then
		get_value "fw_version"
	else
		echo ""
	fi
	print_value $INIT_SPACE "Firmware Version" "$QL_ATTR_VALUE"
	if [ $QL_FS == $QL_SYS ]; then
		get_value "state"
	else
		echo ""
	fi
	print_value $INIT_SPACE "Link State" "$QL_ATTR_VALUE"
fi
}
# --------------------------------------------------------- #
# common_display ()                                         #
# Displays common parameters for HBA's                      #
# After version 1.4, Modified for proc too                  #
# Parameters :                                              #
#              NONE                                         #
# --------------------------------------------------------- #
function common_display
{
	
	if [ $QL_FS == $QL_SYS ]; then
		if [ -e /sys/class/fc_host/host$HOST ]; then
			cd /sys/class/fc_host/host$HOST
		fi
	else
####		if [ -e /proc/scsi/$QL_MODULE/$HOST ]; then
		if [ -e ${QL_PROC_PATH}/$HOST ]; then
####			cd  /proc/scsi/$QL_MODULE/
			cd ${QL_PROC_PATH} 
		fi
	fi

	if [ $QL_FS == $QL_SYS ]; then
		get_value "port_name"
	else
		QL_ATTR_VALUE=`cat $HOST | grep "scsi-.*-adapter-port" | sed "s/.*=\(.*\);/\1/"`
	fi
	print_value $INIT_SPACE "WWPN" "$QL_ATTR_VALUE"
	
	if [ $QL_FS == $QL_SYS ]; then
		get_value "node_name"
	else
		QL_ATTR_VALUE=`cat $HOST | grep "scsi-.*-adapter-node" | sed "s/.*=\(.*\);/\1/"`
	fi
	print_value $INIT_SPACE "WWNN" "$QL_ATTR_VALUE"

	if [ $QL_FS == $QL_SYS ] && [ -e /sys/class/scsi_host/host$HOST/ ]; then
		cd /sys/class/scsi_host/host$HOST/
	fi
	
	if [ $QL_FS == $QL_SYS ]; then
		get_value "driver_version"
	else
		QL_ATTR_VALUE=`cat $HOST | grep "Driver" | sed "s/.*Driver\ version\ \(.*\)/\1/"`
	fi
	print_value $INIT_SPACE "Driver Version" "$QL_ATTR_VALUE"

	if [ $QL_FS == $QL_SYS ]; then
		get_value "model_name"
	else
		QL_ATTR_VALUE=`cat $HOST | grep "QLogic.* Host Adapter for " | sed "s/.*QLogic.*Host Adapter for \(.*\):/\1/"`

	fi
        print_value $INIT_SPACE "Model" "$QL_ATTR_VALUE"

	if [ $QL_FS == $QL_SYS ]; then
		get_value "fw_version" 
	else
		QL_ATTR_VALUE=`cat $HOST | grep "Firmware version" | sed "s/.*Firmware version \(.*\),.*/\1/"`
		if [ "$QL_ATTR_VALUE" == "" ]; then
			QL_ATTR_VALUE="Unknown"
		fi
	fi
	print_value $INIT_SPACE "Firmware Version" "$QL_ATTR_VALUE"
	
	if [ $QL_FS == $QL_SYS ]; then
		get_value "state"
	else
		QL_ATTR_VALUE=`cat $HOST | grep "loop state" | sed "s/.*loop state.*<\(.*\)>.*/\1/"`
		if [ "$QL_ATTR_VALUE" == "" ]; then
			QL_ATTR_VALUE="Unknown"
		fi
	fi
	print_value $INIT_SPACE "Link State" "$QL_ATTR_VALUE"
	
	# In Sysfs these values are printed in verbose option
	if [ $QL_FS == $QL_PROC ]; then
		QL_ATTR_VALUE=`cat $HOST | grep "Device queue depth" | sed "s/.*=[[:space:]]\+\(.*\)/\1/"`
		print_value $INIT_SPACE "Device queue depth" "$QL_ATTR_VALUE"
	fi

	if [ $QL_FS == $QL_PROC ]; then
	cat $HOST | grep "Number of reqs in pending" | sed "s/.*pending_q=\(.*\),.*retry_q=\(.*\),.*done_q=\(.*\),.*scsi_retry_q=\(.*\)/Req in Pending Queue:\1\nReq in Retry Queue:\2\nReq in Done Queue:\3\nReq in SCSI Retry Queue:\4/"	
	fi
}

# --------------------------------------------------------- #
# print_host_headline ()                                    #
# Prints heading for display                                #
# Parameters :                                              #
#              Index                                        #
# Returns :                                                 #
#		NONE                                        #
# --------------------------------------------------------- #
function print_host_headline 
{
echo_b "QLogic HBA (FC/FCoe) Host$HOST Information"
echo ""
}

# --------------------------------------------------------- #
# print_host_headline ()                                    #
# Prints heading for display                                #
# Parameters :                                              #
#              Index                                        #
# Returns :                                                 #
#               NONE                                        #
# --------------------------------------------------------- #
function print_iscsi_host_headline
{
echo_b "QLogic HBA (iSCSI) Host$ISCSI_HOST Information "
echo ""
}

# --------------------------------------------------------- #
# print_target_info ()                                      #
# Prints target information for given HOST and Target       #
# Parameters :                                              #
#              HOST and TARGET                              #
# --------------------------------------------------------- #
function print_target_info
{
HOST=$1
TARGET=$2

	if [ $QL_FS == $QL_SYS ]; then
		if [ -e /sys/class/fc_transport/target$HOST:0:${TARGET}/device/${HOST}:0:${TARGET}:0 ]; then
			cd /sys/class/fc_transport/target$HOST:0:${TARGET}/device/${HOST}:0:${TARGET}:0
####		if [ -e /sys/class/scsi_host/host$HOST/device/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:0 ]; then
####			cd /sys/class/scsi_host/host$HOST/device/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:0
####		elif [ -e /sys/class/scsi_host/host$HOST/device/rport-$HOST:0-$TARGET/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:0 ]; then
####		cd /sys/class/scsi_host/host$HOST/device/rport-$HOST:0-$TARGET/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:0
		fi
	

	else
###		if [ -e /proc/scsi/$QL_MODULE/$HOST ]; then
		if [ -e ${QL_PROC_PATH}/$HOST ]; then
###			cd  /proc/scsi/$QL_MODULE/
			cd ${QL_PROC_PATH} 
		fi
	fi

		echo_b "Device : $TARGET"
		echo ""

	if [ $QL_FS == $QL_SYS ]; then
		get_value "vendor"
	else
		cat /proc/scsi/scsi | grep -n el > $QL_TEMP
		LINE=`cat $QL_TEMP | grep "$HOST Channel: 00 Id:.*$TARGET Lun:" -m 1 | sed "s/\(.*\):Host.*/\1/"`
		LINE=$(($LINE+1))

		QL_ATTR_VALUE=`cat $QL_TEMP | grep "^$LINE:" | sed "s/.*Vendor:\(.*\)[[:space:]]\+Rev.*/\1/"`
		if [ "$QL_ATTR_VALUE" == "" ]; then
			QL_ATTR_VALUE="Unknown"
		fi
		TEMP_MODEL=`cat $QL_TEMP | grep "^$LINE:" | sed "s/.*Model:\(.*\)[[:space:]]\+Rev.*/\1/"`
		if [ "$TEMP_MODEL" == "" ]; then
			TEMP_MODEL="Unknown"
		fi
		
		rm -f $QL_TEMP
	fi

	print_value $INIT_SPACE "Vendor" $QL_ATTR_VALUE
	
	if [ $QL_FS == $QL_SYS ]; then
		get_value "model"
	else
		QL_ATTR_VALUE=$TEMP_MODEL
	fi
	
	print_value $INIT_SPACE "Model" $QL_ATTR_VALUE
	
	if [ $QL_FS == $QL_SYS ]; then

		if [ -e "/sys/class/fc_transport/target${HOST}:0:${TARGET}/device/fc_transport:target${HOST}:0:${TARGET}" ]; then
			cd /sys/class/fc_transport/target${HOST}:0:${TARGET}/device/fc_transport:target${HOST}:0:${TARGET}/
		
		elif [ -e "/sys/class/fc_transport/target${HOST}:0:${TARGET}/device/fc_transport/target${HOST}:0:${TARGET}" ]; then
			cd /sys/class/fc_transport/target${HOST}:0:${TARGET}/device/fc_transport/target${HOST}:0:${TARGET}/	
		fi

		get_value "port_name"
	else 
		QL_ATTR_VALUE=`cat $HOST | grep "scsi-.*target-${TARGET}" | sed "s/.*=\(.*\);/\1/"`

		if [ "$QL_ATTR_VALUE" != "" ]; then
			TEMP=$QL_ATTR_VALUE
			QL_ATTR_VALUE="0x${QL_ATTR_VALUE}"
			
		fi
	fi
	print_value $INIT_SPACE "WWPN" "$QL_ATTR_VALUE"

	 if [ $QL_FS == $QL_SYS ]; then
                if [ -e /sys/class/fc_transport/target${HOST}:0:${TARGET} ]; then
                        cd /sys/class/fc_transport/target${HOST}:0:${TARGET}
                fi
                get_value "port_id"
        else
                QL_ATTR_VALUE=`cat $HOST | grep "scsi-.*port.*=.*:${TEMP}" | sed "s/.*=.*:.*:\(.*\):.*;/\1/"`                
		if [ "$QL_ATTR_VALUE" != "" ]; then
                        QL_ATTR_VALUE="0x${QL_ATTR_VALUE}"
		else
			QL_ATTR_VALUE="Unknown"
                fi
        fi

        print_value $INIT_SPACE "Port ID" "$QL_ATTR_VALUE"
	
	if [ $QL_FS == $QL_SYS ]; then
		get_luns $HOST $TARGET
		LUN_QTY=$?
	else
	        get_proc_luns $HOST $TARGET
		LUN_QTY=$?
	fi
	
	if [ $LUN_QTY -gt 0 ]; then 
		QL_ATTR_VALUE=$LUN_QTY	
	else
		QL_ATTR_VALUE="NONE"
	fi	
	print_value $INIT_SPACE "Number of LUNs" $QL_ATTR_VALUE
	cd ../../../..
}



# --------------------------------------------------------- #
# print_target_info ()                                      #
# Prints target information for given HOST and Target       #
# Parameters :                                              #
#              HOST and TARGET                              #
# --------------------------------------------------------- #
function print_iscsi_target_info
{
HOST=$1
TARGET=$2

	if [ $QL_FS == $QL_SYS ]; then
		if [ -e /sys/class/iscsi_host/host$HOST/device/session*/target$HOST:*:$TARGET ]; then
			cd  /sys/class/iscsi_host/host$HOST/device/session*/target$HOST:*:$TARGET/$HOST:*:$TARGET:*/
		fi
	fi
	if [ $QL_FS == $QL_SYS ]; then
		cd ../../iscsi_session/session*/
		get_value "targetname"
		cd - >& /dev/null
        else
               	echo " "
        fi
	print_value $INIT_SPACE "targetname" $QL_ATTR_VALUE

        if [ $QL_FS == $QL_SYS ]; then
                get_value "state"
        else
               	echo " "
        fi
	print_value $INIT_SPACE "state" $QL_ATTR_VALUE


	if [ $QL_FS == $QL_SYS ]; then
                get_value "vendor"
        else
                echo " "
        fi
        print_value $INIT_SPACE "vendor" $QL_ATTR_VALUE
}

# --------------------------------------------------------- #
# print_lun_heading ()
# Prints caption to lun information, calculates the space by
# reading length of the values
# Parameters : 
#                HOST and TARGET
# --------------------------------------------------------- #
function print_lun_heading ()
{
local HOST=$1
local TARGET=$2
local M_SPACE=0

if [ -e /sys/class/scsi_device/${HOST}:0:${TARGET}:0/device ]; then
	cd /sys/class/scsi_device/${HOST}:0:${TARGET}:0/device/
else 
	return 1
fi

for VALUE in `cat model 2> /dev/null | sed "s/ /./g"`
do
	if [ ${#VALUE} -gt $M_SPACE ]; then
		M_SPACE=`echo ${#VALUE}`
	fi
done
M_SPACE=$((M_SPACE - 4))
echo_b "[H:C:T:L]\tType\tVendor\t  Model"

while [ $M_SPACE -ge 0 ]; 
do	
	echo -en " "
	M_SPACE=$((M_SPACE - 1))
done
echo_b "Rev   Block Dev\tGeneric Dev"
}


# --------------------------------------------------------- #
# print_iscsi_lun_heading ()
# Prints caption to lun information, calculates the space by
# reading length of the values
# Parameters :
#                HOST and TARGET
# --------------------------------------------------------- #
function print_iscsi_lun_heading ()
{
local ISCSI_HOST=$1
local TARGET=$2
local M_SPACE=0
local iscsi_luns=()

if [ -e /sys/class/scsi_device/ ]; then
	cd /sys/class/scsi_device/
	iscsi_luns=( `ls -d ${ISCSI_HOST}:0:${TARGET}:*` )
	echo ${iscsi_luns[@]} | grep -w $1 &> /dev/null
	if [ $? -eq 0 ]; then
		cd ${iscsi_luns[0]}/device
	else
		return 1
	fi
else
        return 1
fi

for VALUE in `cat model 2> /dev/null | sed "s/ /./g"`
do
        if [ ${#VALUE} -gt $M_SPACE ]; then
                M_SPACE=`echo ${#VALUE}`
        fi
done
M_SPACE=$((M_SPACE - 4))
echo_b "[H:C:T:L]\tType\tVendor\t  Model"

while [ $M_SPACE -ge 0 ];
do
        echo -en " "
        M_SPACE=$((M_SPACE - 1))
done
echo_b "Rev   Block Dev\tGeneric Dev"
}


# --------------------------------------------------------- #
# print_proc_lun_heading ()
# Prints caption to lun information, calculates the space by
# reading calculating length of Model value
# Parameters : 
#                HOST 
# --------------------------------------------------------- #


function print_proc_lun_heading
{
local HOST=$1
local M_SPACE=0
local T_SPACE=0

MODEL_LENGTH=`cat /proc/scsi/scsi | grep scsi$HOST -A 2 | grep "Model:.*Rev" -m 1 -o | wc -m`
MODEL_LENGTH=$((MODEL_LENGTH-18))
TYPE_LENGTH=`cat /proc/scsi/scsi | grep scsi$HOST -A 2 | grep "Type:.*ANSI" -m 1 -o | wc -m`
TYPE_LENGTH=$((TYPE_LENGTH-30))

M_SPACE=$MODEL_LENGTH
T_SPACE=$((TYPE_LENGTH-4))

echo_b "[H:C:T:L]\tType"
while [ $T_SPACE -ge 0 ]; do	
	echo -en " "
	T_SPACE=$((T_SPACE - 1))
done

echo_b "Vendor\t\tModel"
while [ $M_SPACE -ge 0 ]; do	
	echo -en " "
	M_SPACE=$((M_SPACE - 1))
done
echo_b "Rev"
}

# --------------------------------------------------------- #
# print_all_details ()                                      #
# Prints all details of given host                          #
# Parameters :                                              #
#              HOST / ALL                                   #
# Returns :                                                 #
#             NONE                                          #
# --------------------------------------------------------- #
function print_all_details
{
local OPTION=$1

local HOST_LIST=()
echo $1 | grep "^[0-9]" &> /dev/null
if [ $? -eq 0 ]; then
	echo ${QL_HOST[@]} | grep -w $1 &> /dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: HOST $1 not found on the system"
		exit 1
	else
		HOST_LIST=($1)
	fi
elif [ $OPTION == "ALL" ]; then
	HOST_LIST=( `echo ${QL_HOST[@]}` )
else
	exit 1
fi


for HOST in ${HOST_LIST[@]}
do
	if [ -e /sys/class/fc_host/host$HOST/ ]; then
		cd /sys/class/fc_host/host$HOST/
	fi
	echo_b "QLogic HBA (FC/FCoe) Host$HOST Information "
	echo ""
	get_value "port_name"
	print_value 0 "WWPN" $QL_ATTR_VALUE

	get_value "node_name"
	print_value 0 "WWNN" $QL_ATTR_VALUE

	if [ -e /sys/class/scsi_host/host$HOST/ ]; then
		cd /sys/class/scsi_host/host$HOST/
	fi
	get_value "driver_version"
	print_value 0 "Driver Version" $QL_ATTR_VALUE

	get_value "fw_version"
	print_value 0 "Firmware" $QL_ATTR_VALUE

	get_value "state"
	print_value 0 "Link State" "$QL_ATTR_VALUE"

	if [ -e /sys/class/fc_host/host$HOST/ ]; then
		cd /sys/class/fc_host/host$HOST/
	fi

	get_value "speed"
	if [ "${QL_ATTR_VALUE}" == "unknown" ] || [ "${QL_ATTR_VALUE}" == "Unknown" ]; then
		print_value 0 "Speed" "${QL_ATTR_VALUE}"
	else
		print_value 0 "Speed" "${QL_ATTR_VALUE}/s"
	fi

	get_value "port_id"
	print_value 0 "Port ID" $QL_ATTR_VALUE

	get_value "port_type"
	print_value 0 "Port Type" $QL_ATTR_VALUE

	local CLASS=()

	CLASS=(`cat supported_classes 2> /dev/null | sed "s/$/EnD/g"`)	
	CLASS=( `echo ${CLASS[@]} | sed "s/EnD/, /g"`)
	CLASS=( `echo ${CLASS[@]} | sed "s/,$//"` ) 
	echo "Supported Classes: ${CLASS[@]}"

	#QL_TABS=0
	get_value "tgtid_bind_type"
	print_value 0 "Target Bind Type" $QL_ATTR_VALUE
	#QL_TABS=2

	if [ -e "/sys/class/fc_host/host$HOST/device/scsi_host:host$HOST/" ]; then
		cd /sys/class/fc_host/host$HOST/device/scsi_host:host$HOST/

	elif [ -e "/sys/class/fc_host/host$HOST/device/scsi_host/host$HOST/" ]; then
		cd /sys/class/fc_host/host$HOST/device/scsi_host/host$HOST/
	fi

	get_value "zio"
	print_value 0 "ZIO State" $QL_ATTR_VALUE

	get_value "zio_timer"
	print_value 0 "ZIO Timer" "$QL_ATTR_VALUE"

	get_value "beacon"
	print_value 0 "Beacon State" "$QL_ATTR_VALUE"

	#QL_TABS=2
	get_targets $HOST
	if [ $? -ne 0 ]; then
		echo_b "Device Information"
		#echo ""
	fi
	for TARGET in ${QL_TARGETS[@]}
	do
		if [ -e /sys/class/fc_transport/target$HOST:0:$TARGET ]; then
			cd /sys/class/fc_transport/target$HOST:0:$TARGET
		fi
		echo_b "FC_RPORT = ($HOST:0:$TARGET)"
		echo ""

		get_value "port_id"
		print_value $INIT_SPACE "Port ID" $QL_ATTR_VALUE

		get_value "port_name"
		print_value $INIT_SPACE "WWPN" $QL_ATTR_VALUE

		get_value "node_name"
		print_value $INIT_SPACE "WWNN" $QL_ATTR_VALUE
			
		get_luns $HOST $TARGET
		if [ $? -ne 0 ]; then
			echo_b "LUN Information [FC/FCoe]"
			echo ""
			print_lun_heading $HOST $TARGET
			echo ""	
		fi

		QL_SORTED_LUNS=(`echo ${QL_LUNS[@]} | sed "s/ /\\n/g" | sort -n`)
		for LUN in ${QL_SORTED_LUNS[@]}
		do
####			if [ -e /sys/class/scsi_host/host$HOST/device/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:${LUN} ]; then
####				cd /sys/class/scsi_host/host$HOST/device/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:${LUN}
			if [ -e /sys/class/scsi_device/${HOST}:0:${TARGET}:${LUN}/device ]; then
				cd /sys/class/scsi_device/${HOST}:0:${TARGET}:${LUN}/device/
####			elif [ -e /sys/class/scsi_host/host$HOST/device/rport-$HOST:0-$TARGET/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:${LUN} ]; then
####				cd /sys/class/scsi_host/host$HOST/device/rport-$HOST:0-$TARGET/target${HOST}:0:${TARGET}/${HOST}:0:${TARGET}:${LUN}
			fi
			local MAJOR
			local MINOR
			echo_b "[$HOST:0:$TARGET:$LUN]"
			get_value "type"
			echo -en "\t${DEVICE_TYPES[$QL_ATTR_VALUE]}"
			get_value "vendor"
			echo -en "\t$QL_ATTR_VALUE"
			get_value "model"
			echo -en "  $QL_ATTR_VALUE"
			get_value "rev"
			echo -en "  $QL_ATTR_VALUE"

			get_value "block*/dev"
			MAJOR=`echo $QL_ATTR_VALUE | cut -d : -f 1` 
			MINOR=`echo $QL_ATTR_VALUE | cut -d : -f 2` 
			
			DEV_NAME=`ls -l /dev/ | grep -w "$MAJOR,[[:space:]]\+$MINOR" | sed "s/.* \(.*$\)/\/dev\/\1/"`	
			if [ ${#DEV_NAME} -eq 0 ]; then	
				echo -en "          "
			else
				echo -en "  $DEV_NAME"
			fi
			get_value "scsi_generic*/dev"
			MAJOR=`echo $QL_ATTR_VALUE | cut -d : -f 1` 
			MINOR=`echo $QL_ATTR_VALUE | cut -d : -f 2` 

			GDEV_NAME=`ls -l /dev/ | grep -w "$MAJOR,[[:space:]]\+$MINOR" | sed "s/.* \(.*$\)/\/dev\/\1/"`
			echo -e "\t$GDEV_NAME"
			if [ $VERBOSE -eq 1 ]; then	
				verbose_attributes $HOST $TARGET $LUN

####				get_value "state"
####				echo -n "State:$QL_ATTR_VALUE "

####				get_value "timeout"
####				echo -n "  Timeout:$QL_ATTR_VALUE "
			
				#QL_TABS=1	
####				get_value "queue_depth"
####				echo -n "  Queue Depth:$QL_ATTR_VALUE "
				#QL_TABS=2
####				if [ -f iorequest_cnt ] && [ -f iodone_cnt ] && [ -f ioerr_cnt ]; then
####				let REQ=`cat iorequest_cnt`
####				let DONE=`cat iodone_cnt` 
####				let ERROR=`cat ioerr_cnt` 
				

#####				echo -n "   Counts: "
#####				echo -n "Req=$REQ,"
#####				echo -n "Done=$DONE," 
#####				echo "Error=$ERROR"
#####				fi
#####				echo ""
			fi

		done


	done

done
}



# --------------------------------------------------------- #
# print_iscsi_details ()                                    #
# Prints all details of given host                          #
# Parameters :                                              #
#              HOST / ALL                                   #
# Returns :                                                 #
#             NONE                                          #
# --------------------------------------------------------- #
function print_iscsi_details
{
local OPTION=$1

local HOST_LIST=()
echo $1 | grep "^[0-9]" &> /dev/null
if [ $? -eq 0 ]; then
	echo ${QL_ISCSI_HOST[@]} | grep -w $1 &> /dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: HOST $1 not found on the system"
		exit 1
	else
		HOST_LIST=($1)
	fi
elif [ $OPTION == "ALL" ]; then
	HOST_LIST=( `echo ${QL_ISCSI_HOST[@]}` )
else
	exit 1
fi


for ISCSI_HOST in ${HOST_LIST[@]}
do
	if [ -e /sys/class/iscsi_host/host$ISCSI_HOST ]; then
		cd /sys/class/iscsi_host/host$ISCSI_HOST
	fi

	echo ""
	echo_b "QLogic HBA (iSCSI) Host$ISCSI_HOST Information "
	echo ""
	get_value "initiatorname"
	print_value $INIT_SPACE "initiatorname" "$QL_ATTR_VALUE"
	get_value "ipaddress"
	print_value $INIT_SPACE "ip address" "$QL_ATTR_VALUE"
	get_value "hwaddress"
	print_value $INIT_SPACE "H/w address" "$QL_ATTR_VALUE"

	if [ -e /sys/module/qla4xxx/ ]; then
		cd /sys/module/qla4xxx/
        fi

	get_value "version"
	print_value $INIT_SPACE "driver version" "$QL_ATTR_VALUE"

	get_iscsi_targets $ISCSI_HOST
	if [ $? -ne 0 ]; then
		echo_b "Device Information"
	fi

	for TARGET in ${QL_ISCSI_TARGETS[@]}
	do
		echo " "
		if [ -e /sys/class/iscsi_host/host$ISCSI_HOST/device/session*/target$ISCSI_HOST:*:$TARGET ]; then
			cd  /sys/class/iscsi_host/host$ISCSI_HOST/device/session*/iscsi_session/session*/
		fi
                get_value "targetname"
		print_value $INIT_SPACE "targetname" $QL_ATTR_VALUE
                get_value "state"
		print_value $INIT_SPACE "state" $QL_ATTR_VALUE
		cd /sys/class/iscsi_host/host$ISCSI_HOST/device/session*/\
		target$ISCSI_HOST:*:$TARGET/$ISCSI_HOST:*:$TARGET:*/
		get_value "vendor"
		print_value $INIT_SPACE "vendor" $QL_ATTR_VALUE
		get_iscsi_luns $ISCSI_HOST $TARGET
		if [ $? -ne 0 ]; then
			echo_b "LUN Information [iSCSI]"
			echo ""
			print_iscsi_lun_heading $ISCSI_HOST $TARGET
			echo ""
		fi

		QL_SORTED_LUNS=(`echo ${QL_ISCSI_LUNS[@]} | \
		sed "s/ /\\n/g" | sort -n`)
		for LUN in ${QL_SORTED_LUNS[@]}
		do
			if [ -e /sys/class/scsi_device/${ISCSI_HOST}:0:${TARGET}:${LUN}/device ]; then
				cd /sys/class/scsi_device/${ISCSI_HOST}:0:${TARGET}:${LUN}/device/
			fi
			local MAJOR
			local MINOR
			echo_b "[$ISCSI_HOST:0:$TARGET:$LUN]"
			get_value "type"
			echo -en "\t${DEVICE_TYPES[$QL_ATTR_VALUE]}"
			get_value "vendor"
			echo -en "\t$QL_ATTR_VALUE"
			get_value "model"
			echo -en "  $QL_ATTR_VALUE"
			get_value "rev"
			echo -en "  $QL_ATTR_VALUE"

			get_value "block*/dev"
			MAJOR=`echo $QL_ATTR_VALUE | cut -d : -f 1`
			MINOR=`echo $QL_ATTR_VALUE | cut -d : -f 2`
			DEV_NAME=`ls -l /dev/ | grep -w "$MAJOR,[[:space:]]\+\
			$MINOR" | sed "s/.* \(.*$\)/\/dev\/\1/"`
			if [ ${#DEV_NAME} -eq 0 ]; then
				echo -en "          "
			else
				echo -en "  $DEV_NAME"
			fi
			get_value "scsi_generic*/dev"
			MAJOR=`echo $QL_ATTR_VALUE | cut -d : -f 1`
			MINOR=`echo $QL_ATTR_VALUE | cut -d : -f 2`
			GDEV_NAME=`ls -l /dev/ | grep -w "$MAJOR,[[:space:]]\+\
			$MINOR" | sed "s/.* \(.*$\)/\/dev\/\1/"`
			echo -e "\t$GDEV_NAME"
			if [ $VERBOSE -eq 1 ]; then
				verbose_iscsi_attributes $ISCSI_HOST $TARGET\
				$LUN
			fi

		done
	done
done
}




# --------------------------------------------------------- #
# verbose_attributes ()                                     #
# prints the verbose attributes of the scsi device          #
# Parameters :                                              #
#		HOST,TARGET,LUN                             #
# Returns :                                                 #
# 		None                                        #
# --------------------------------------------------------- #
function verbose_attributes()
{
	HOST=$1 
	TARGET=$2 
	LUN=$3

	if [ -e /sys/class/scsi_device/${HOST}:0:${TARGET}:${LUN}/device ]; then
		cd /sys/class/scsi_device/${HOST}:0:${TARGET}:${LUN}/device/
	fi

	if [ -e state ]; then # change state file for REDHAT
		get_value "state"
		echo -n "State:$QL_ATTR_VALUE "
	elif [ -e online ]; then
		if [ `cat online` == "0" ]; then
			echo -n "State:offline "
		elif [ `cat online` == "1" ]; then
			echo -n "State:running "
		fi
	fi

	get_value "timeout"
	echo -n "  Timeout:$QL_ATTR_VALUE "
	
	#QL_TABS=1	
	get_value "queue_depth"
	echo -n "  Queue Depth:$QL_ATTR_VALUE "
	#QL_TABS=2
	if [ -f iorequest_cnt ] && [ -f iodone_cnt ] && [ -f ioerr_cnt ]; then
		let REQ=`cat iorequest_cnt`
		let DONE=`cat iodone_cnt` 
		let ERROR=`cat ioerr_cnt` 

		echo -n "   Counts: "
		echo -n "Req=$REQ,"
		echo -n "Done=$DONE," 
		echo "Error=$ERROR"
	fi
	echo ""
}


# --------------------------------------------------------- #
# verbose_iscsi_attributes ()                               #
# prints the verbose attributes of the scsi device          #
# Parameters :                                              #
#		HOST,TARGET,LUN                             #
# Returns :                                                 #
# 		None                                        #
# --------------------------------------------------------- #
function verbose_iscsi_attributes()
{
        ISCSI_HOST=$1
        TARGET=$2
        LUN=$3

        if [ -e /sys/class/scsi_device/${ISCSI_HOST}:0:${TARGET}:${LUN}/device ]; then
                cd /sys/class/scsi_device/${ISCSI_HOST}:0:${TARGET}:${LUN}/device/
        fi

        if [ -e state ]; then # change state file for REDHAT
                get_value "state"
                echo -n "State:$QL_ATTR_VALUE "
        elif [ -e online ]; then
                if [ `cat online` == "0" ]; then
                        echo -n "State:offline "
                elif [ `cat online` == "1" ]; then
                        echo -n "State:running "
                fi
        fi

        get_value "timeout"
        echo -n "  Timeout:$QL_ATTR_VALUE "

        get_value "queue_depth"
        echo -n "  Queue Depth:$QL_ATTR_VALUE "
        if [ -f iorequest_cnt ] && [ -f iodone_cnt ] && [ -f ioerr_cnt ]; then
                let REQ=`cat iorequest_cnt`
                let DONE=`cat iodone_cnt`
                let ERROR=`cat ioerr_cnt`

                echo -n "   Counts: "
                echo -n "Req=$REQ,"
                echo -n "Done=$DONE,"
                echo "Error=$ERROR"
        fi
        echo ""
}

# --------------------------------------------------------- #
# print_proc_all_details ()                                 #
# prints the all attributes of the  HOST,TARGET,LUN         #
# Parameters :                                              #
#		 HOST list                            	    #
# Returns :                                                 #
# 		None                                        #
# --------------------------------------------------------- #
function print_proc_all_details
{
local OPTION=$1
local HOST_LIST=()

echo $1 | grep "^[0-9]" &> /dev/null
if [ $? -eq 0 ]; then
	echo ${QL_HOST[@]} | grep -w $1 &> /dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: HOST $1 not found on the system"
		exit 1
	else
		HOST_LIST=($1)
	fi
elif [ $OPTION == "ALL" ]; then
	HOST_LIST=( `echo ${QL_HOST[@]}` )
else
	exit 1
fi

for HOST in ${HOST_LIST[@]}
do
	print_host_headline
	common_display $HOST
	get_proc_targets $HOST
	if [ $? -ne 0 ]; then
		echo_b "Device Information"
		#echo ""
	fi

	for TARGET in ${QL_TARGETS[@]}
	do
		echo_b "FC_RPORT = ($HOST:0:$TARGET)"
		echo ""

		QL_ATTR_VALUE=`cat $HOST | grep "scsi-qla.*target-${TARGET}" | sed "s/.*=\(.*\);/\1/"`
		if [ "${QL_ATTR_VALUE}" != "" ]; then
			TEMP=$QL_ATTR_VALUE
			QL_ATTR_VALUE="0x${QL_ATTR_VALUE}"
		fi
		print_value $INIT_SPACE "WWPN" "$QL_ATTR_VALUE"

		QL_ATTR_VALUE=`cat $HOST | grep "scsi-.*port.*=.*:${TEMP}" | sed "s/.*=\(.*\):.*:.*:.*;/\1/"`

		if [ "${QL_ATTR_VALUE}" != "" ]; then
			QL_ATTR_VALUE="0x${QL_ATTR_VALUE}"
		fi
		print_value $INIT_SPACE "WWNN" "$QL_ATTR_VALUE"
	
		QL_ATTR_VALUE=`cat $HOST | grep "scsi-.*port.*=.*:${TEMP}" | sed "s/.*=.*:.*:\(.*\):.*;/\1/"`
                     if [ "${QL_ATTR_VALUE}" != "" ]; then
                             QL_ATTR_VALUE="0x${QL_ATTR_VALUE}"
                     fi
                     print_value $INIT_SPACE "Port ID" "$QL_ATTR_VALUE"

	
		get_proc_luns $HOST $TARGET
		if [ $? -ne 0 ]; then
			echo_b "LUN Information"
			echo ""
			print_proc_lun_heading $HOST
			echo ""
		else
			echo ""
			return 0
		fi
		
		for LUN in ${QL_LUNS[@]}
		do
		echo_b "[$HOST:0:$TARGET:$LUN]"

		if [ $LUN -le 9 ]; then
		TEMP=`cat /proc/scsi/scsi | grep "scsi$HOST.*Id:[[:space:]].${TARGET}.*Lun:[[:space:]]0${LUN}" -A 2 |  grep "Type:" | sed "s/Type:[[:space:]]\+\(.*\)[[:space:]]\+ANSI.*/\1/" | sed "s/ //g"`
		else
		TEMP=`cat /proc/scsi/scsi | grep "scsi$HOST.*Id:[[:space:]].${TARGET}.*Lun:[[:space:]]${LUN}" -A 2 |  grep "Type:" | sed "s/Type:[[:space:]]\+\(.*\)[[:space:]]\+ANSI.*/\1/" | sed "s/ //g"`
		fi
	#echo "cat /proc/scsi/scsi | grep scsi$HOST.*Id:[[:space:]].${TARGET}.*Lun:[[:space:]]${LUN} -A 2 |  grep Type: | sed s/Type:[[:space:]]\+\(.*\)[[:space:]]\+ANSI.*/\1/ | sed s/ //g"
	#	cat /proc/scsi/scsi | grep "scsi$HOST.*Id:.*${TARGET}.*Lun:[[:space:]]${LUN}" -A 2 |  grep "Type:.*Rev:" -o | sed "s/Model:[[:space:]]\+\(.*\)[[:space:]]\+Rev:/\1/"
		echo -en "\t${TEMP}"
		TEMP_COUNT=`echo $TEMP | wc -m`
		TEMP_COUNT=$((TYPE_LENGTH-TEMP_COUNT-1))
		while [ $TEMP_COUNT -ge 0 ]; 
		do	
			echo -en " "
			TEMP_COUNT=$((TEMP_COUNT - 1))
		done

		if [ $LUN -le 9 ]; then
			cat /proc/scsi/scsi | grep "scsi$HOST.*Id:[[:space:]].${TARGET}.*Lun:[[:space:]]0${LUN}" -A 2 |  grep "Vendor:" | sed "s/Vendor:[[:space:]]\+\(.*\)[[:space:]]\+Model:[[:space:]]\+\(.*\)[[:space:]]\+Rev:[[:space:]]\+\(.*\)/\1\t\2\3/"
		else
			cat /proc/scsi/scsi | grep "scsi$HOST.*Id:[[:space:]].${TARGET}.*Lun:[[:space:]]${LUN}" -A 2 |  grep "Vendor:" | sed "s/Vendor:[[:space:]]\+\(.*\)[[:space:]]\+Model:[[:space:]]\+\(.*\)[[:space:]]\+Rev:[[:space:]]\+\(.*\)/\1\t\2\3/"
		fi

		if [ $VERBOSE -eq 1 ]; then	
			verbose_attributes $HOST $TARGET $LUN
		fi

		done 
		echo ""	

	done

done
}

# --------------------------------------------------------- #
# print_parameters ()                                       #
# Prints all the parameters for FC host                     #
# Parameters :                                              #
#               NONE                                        #
# Returns :                                                 #
#              NONE                                         #
# --------------------------------------------------------- #
function print_parameters 
{
	if [ -e /sys/module/qla2xxx/parameters ]; then
		cd /sys/module/qla2xxx/parameters
	elif [ -e /sys/module/qla2xxx/ ]; then
		cd /sys/module/qla2xxx/
	else 
		return
	fi
		echo_b "1. FC HBA Parameters"
		echo ""
		echo ""
		get_value "ql2xlogintimeout"
		print_value $INIT_SPACE "Login Timeout" $QL_ATTR_VALUE

		get_value "qlport_down_retry"
		print_value $INIT_SPACE "Port Down Retry" $QL_ATTR_VALUE

		get_value "ql2xplogiabsentdevice"
		print_value $INIT_SPACE "Plogi Absent Device" $QL_ATTR_VALUE
		
		get_value "ql2xloginretrycount"
		print_value $INIT_SPACE "Login Retry Count" $QL_ATTR_VALUE

		get_value "ql2xfwloadflash"
		print_value $INIT_SPACE "Load Firmware" $QL_ATTR_VALUE

		get_value "ql2xfdmienable"
		print_value $INIT_SPACE "FDMI Enabled" $QL_ATTR_VALUE
		
		get_value "ql2xprocessrscn"
		print_value $INIT_SPACE "Process RSCN" $QL_ATTR_VALUE
	
		if [ -f "refcnt" ]; then
			get_value "refcnt"
		else
			get_value "../refcnt"
		fi
	
		print_value $INIT_SPACE "Reference Count" $QL_ATTR_VALUE

		echo ""	
}


function print_iscsi_parameters
{
        if [ -e /sys/module/qla4xxx/parameters ]; then
                cd /sys/module/qla4xxx/parameters
        elif [ -e /sys/module/qla4xxx/ ]; then
                cd /sys/module/qla4xxx/
        else
                return
        fi
                echo ""
                echo_b "iSCSI HBA Parameters"
                echo ""
                get_value "ql4xdiscoverywait"
                print_value $INIT_SPACE "Discovery Wait" $QL_ATTR_VALUE

                get_value "ql4xdontresethba"
                print_value $INIT_SPACE "Rest HBA" $QL_ATTR_VALUE

                get_value "ql4xenablemsix"
                print_value $INIT_SPACE "Enable MSI-X" $QL_ATTR_VALUE

                echo ""
}


function print_proc_parameters
{
	HOST=$1
	echo_b "1. FC HBA $HOST Parameters"
	echo ""
	echo ""
	cat $QL_PROC_PATH/$HOST | grep -w retry	
	cat $QL_PROC_PATH/$HOST | grep -w Timeout
	local COUNT=`lsmod | grep -w ^$QL_MODULE | sed "s/.*[[:space:]]\+.*[[:space:]]\+\(.*\)[[:space:]]\+.*/\1/"`
	(( COUNT -= 1 ))
	echo Reference Count = $COUNT
	echo -n "Failover = "
	cat /proc/scsi/qla2xxx/$HOST | grep Driver | sed "s/.*Driver\ version\(.*\)/\1/" | grep "fo" >& /dev/null
	if [ $? == 0 ]; then
		echo "Enabled"
	else
		echo "Disabled"
	fi
	echo ""
	if [ ${#QL_HOST} -ge "2" ]; then
		# for single hba this warning would not carry meaning
		echo "NOTE: These parameters displayed are with reference to HOST$HOST, to display"
		echo "parameters for other HOST run # $QL_SCRIPT --parameters <host number>"
	fi
}


# --------------------------------------------------------- #
# print_all_host ()                                         #
# prints all host list                                      #
# Parameters :                                              #
#                 NONE                                      #
# Returns :                                                 #
#              NONE                                         #
# --------------------------------------------------------- #
function print_all_host
{
echo_b "QLogic HBA FC/FCoe List"
echo""

local HOST
local TEMP=1
for HOST in ${QL_HOST[@]}
do
	echo "$TEMP. HBA $HOST"
	(( TEMP += 1 )) 
done
#echo ""
#echo "To view detailed information of the HBA issue"
#echo "# $QL_SCRIPT <HOST NUMBER>"
#echo ""
#exit 0
}

# --------------------------------------------------------- #
# print_iscsi_host ()                                         #
# prints all host list                                      #
# Parameters :                                              #
#                 NONE                                      #
# Returns :                                                 #
#              NONE                                         #
# --------------------------------------------------------- #
function print_iscsi_host
{
echo_b "QLogic HBA iSCSI List"
echo""

local ISCSI_HOST
local TEMP=1

for ISCSI_HOST in ${QL_ISCSI_HOST[@]}
do
        echo "$TEMP. HBA $ISCSI_HOST"
        (( TEMP += 1 ))
done
echo ""
echo "To view detailed information of the HBA issue"
echo "# $QL_SCRIPT <HOST NUMBER>"
echo ""
}

function display_help
{
echo ""
echo "<Host number> | <-a/--all> [optional]"
echo "                  Provide the <HOST NUMBER> to display the detailed"
echo "                  information of the HBA."
echo "                  With no option specified, general information of all"
echo "                  hosts will be displayed."
echo "                  Ex." 
echo "                  # $QL_SCRIPT <Host Number>"
echo "                  # $QL_SCRIPT --procfs <Host Number> (For procfs)"
echo ""
echo "-h, --help"
echo "                  Prints the help message."
echo "                  Ex."
echo "                  # $QL_SCRIPT --help"
echo ""
echo "-hl, --hostlist"
echo "                  This option displays the list of QLogic hosts (HBAs)"
echo "                  connected to this system."
echo "                  The <HOST NUMBER> can be obtained by using following command"
echo "                  Ex."
echo "                  # $QL_SCRIPT --hostlist"
echo ""
echo "-p, --parameters"
echo "                  This option displays the command line parameters that"
echo "                  can be passed to the QLogic HBA driver."
echo "                  Ex."
echo "                  # $QL_SCRIPT --parameters <Host Number>"
echo ""
echo "-s, --statistics"
echo "                  This option displays the statistical information for"
echo "                  specified host. Stastics option is only supported in"
echo "                  \"sysfs\" based scan."
echo "                  Ex."
echo "                  # $QL_SCRIPT --statistics <Host number/--all>"
echo ""
echo "-proc, --procfs"
echo "                  This options forces the the utility to scans information"
echo "                  based on \"procfs\", instead of default sysfs."
echo "                  Ex."
echo "                  # $QL_SCRIPT --procfs [Other valid option]"
echo ""
echo "-v, --verbose"
echo "                  This enables verbose display. The detailed information"
echo "                  of LUNs will be displayed in addition to standard one."
echo "                  This option is only supported in sysfs."
echo "                  Ex."
echo "                  # $QL_SCRIPT --verbose <Host number/-a>"
echo ""
}

function print_statistics ()
{
HOST=""
local HOST_QTY=$#
if [ -e /sys/class/fc_host ]; then
	cd /sys/class/fc_host
else
	echo "Required information could not be found."
	return 1
fi
for HOST in $@
do
	echo_b "QLogic HBA Host$HOST Statistics"
	echo ""
	if [ -e host$HOST/statistics ]; then
		cd host$HOST/statistics
		for FILE in `ls`
		do
			VALUE=`cat $FILE 2> /dev/null`
			if [ $? -eq 0 ]; then
				echo "$FILE: $VALUE"
			fi
		done
		cd ../..
	else 
		echo "Statistics not available for the Host$HOST"
	fi
	echo ""
done

}


function print_iscsi_statistics ()
{
	for HOST in $@
	do
		echo_b "QLogic HBA Host$HOST Statistics"
        	echo " "
		echo_b "iscsi statistics not supported for HBA : $HOST"
        	echo " "
	done
        echo " "
}


# Start

	KERNEL_VERSION=`uname -r`
	KERNEL_MAJ_MIN=`echo $KERNEL_VERSION | cut -d . -f 1,2`
	if [ $KERNEL_MAJ_MIN != 2.6 ]; then
		echo "Only Kernel version 2.6 and above are supported"
		exit 1
	fi

	QL_MODULES_LOADED=`lsmod | grep -c "^qla"`

	if [ $QL_MODULES_LOADED -eq 0 ]; then
		echo "ERROR: Required QLogic modules not loaded"
		exit 1
	fi

	if [ ! -e ${QL_PROC_PATH} ]; then
		QL_FS=$QL_SYS
	fi 

	ls -d /sys/class/fc_host/host*/device >& /dev/null
	if [ $? -ne 0 ]; then
		QL_FS=$QL_PROC
	fi

	QL_FS=2
	if [ "$1" == "-proc" ] || [ "$1" == "--procfs" ]; then
		QL_FS=$QL_PROC
		shift
	fi

	# Check if desired file system is mounted or not

	if [ ! -e /sys/ ] && [ $QL_FS == $QL_SYS ]; then
		echo "ERROR: /sys does not exist"
		echo "       Please make sure sysfs is mounted"
		exit 1
	fi
	
	if [ ! -e /proc/ ] && [ $QL_FS == $QL_PROC ]; then
		echo "ERROR: /proc does not exist"
		echo "       Please make sure sysfs is mounted"
		exit 1
	fi


	ret_fcoe=0
	ret_iscsi=0
	echo_b "QLogic HBA-Snapshot Utility."		
	echo ""
	if [ $QL_FS == $QL_SYS ]; then
		cd /sys/class/scsi_host >& /dev/null
		qlu_sys_find_all_host
		if [ $? -eq 1 ]; then
			ret_fcoe=1
		fi

		qlu_sys_find_iscsi_host
		if [ $? -eq 1 ]; then
			ret_iscsi=1
		fi
		if [ "$ret_fcoe" -eq "1" ] && [ "$ret_iscsi" -eq "1" ]; then
			echo "exit"
			exit 1
		fi

	else
		cd /proc/scsi/qla2xxx >& /dev/null
		if [ $? -eq 0 ];then
			qlu_proc_find_all_host
		else
			echo_b "QLogic FC HBA not found or proc interface not supported"
			echo ""			
			exit 1
		fi
	fi

	RET=""
	if [ "$1" != "?" ]; then
		# Check if specific host requested
		echo $1 | grep "^[0-9]" &> /dev/null
		RET=$?
	fi
iscsi_flag=0
	if [ "$RET" == "0" ]; then
		HOST_NO=$1
		echo ${QL_HOST[@]} | grep -w $1 &> /dev/null
		if [ $? -ne 0 ]; then
			echo ${QL_ISCSI_HOST[@]} | grep -w $1 &> /dev/null
			if [ $? -ne 0 ]; then
				echo "ERROR: HOST $1 not found on the system"
				exit 1
			else
				iscsi_flag=1
			fi
		fi
		# Probably I should modify this function to support proc
		if [ $QL_FS == $QL_SYS ]; then
			if [ $iscsi_flag -eq 1 ]; then
				print_iscsi_details $HOST_NO | more
			else
				print_all_details $HOST_NO | more
			fi
		else
			QL_FS=$QL_PROC
			print_proc_all_details $HOST_NO | more
		fi
		exit 0
	fi

	# CASE & VALIDATIONS go here
	if [ $# -ne 0 ]; then
	case $1 in
		
		-hl | --hostlist )
		QL_PRINT_LIST=1
		;;

		-p | --parameters )

                if [ $# -ne 2 ]; then
                        echo "Please select proper option"
                        exit 1
                fi
		iscsi_flag=0
                echo $2 | grep "^[0-9]" &> /dev/null
                if [ $? == 0 ]; then
                        echo ${QL_HOST[@]} | grep -w $2 &> /dev/null
                        if [ $? -ne 0 ]; then
                                echo ${QL_ISCSI_HOST[@]} | grep -w $2\
				&> /dev/null
                                if [ $? -ne 0 ]; then
                                        echo "ERROR: HOST $2 not found on the\
					system"
                                        exit 1
                                else
                                        iscsi_flag=1
                                fi
                        fi

                        if [ $iscsi_flag -eq 1 ] ; then
                                print_iscsi_parameters $2 | more
                        else
				print_parameters $2 | more
                        fi
                        exit 0
                fi
		if [ $QL_FS == $QL_SYS ]; then
			print_parameters $2
		else
			if [ "$2" != "" ]; then
				echo ${QL_HOST[@]} | grep -w $2 &> /dev/null
				RET=$?
				if [ "$RET" != "0" ]; then
					echo "ERROR: HOST $2 not found on the\
					system"
					exit 1
				else
					print_proc_parameters $2 
				fi
			else
				print_proc_parameters ${QL_HOST[0]}
			fi
		fi	
		exit 0
		;;

		-h | --help | ? )
		
		display_help | more
		exit 0
		;;
		
		-a | --all  )
		QL_PRINT_ALL_DETAILS=1
		;;

		-v | --verbose )
		if [ $# -ne 2 ]; then
		echo "Please select proper option"
		echo "# $QL_SCRIPT -v <--all/HOST number>"
		exit 1
		fi

####		if [ $QL_FS == $QL_PROC ]; then
####		echo "NOTE: Verbose option is effective only in \"sysfs\" based scanning."
####		echo "      Ignoring verbose option. Press any key to continue...."
####		echo ""
####		read -s -n 1
####		fi
		VERBOSE=1
		iscsi_flag=0
		echo $2 | grep "^[0-9]" &> /dev/null
		if [ $? == 0 ]; then
####			ls -d /sys/class/fc_host/host*/device >& /dev/null
####			if [ $? -eq 0 ]; then
			echo ${QL_HOST[@]} | grep -w $2 &> /dev/null
			if [ $? -ne 0 ]; then
                        	echo ${QL_ISCSI_HOST[@]} |\
				grep -w $2 &> /dev/null
                        	if [ $? -ne 0 ]; then
                                	echo "ERROR: HOST $2 not found on the\
					system"
                                	exit 1
                        	else
                                	iscsi_flag=1
                        	fi
                	fi
			if [ $iscsi_flag -eq 1 ] ; then
				print_iscsi_details $2 | more
			else
				if [ $QL_FS == $QL_SYS ]; then
					print_all_details $2 | more
				else
					QL_FS=$QL_PROC
					print_proc_all_details $2 | more
				fi
			fi
			exit 0
		elif [ $2 == "-a" ] || [ $2 == "--all" ]; then
####			ls -d /sys/class/fc_host/host*/device >& /dev/null
####			if [ $? -eq 0 ]; then
			if [ $QL_FS == $QL_SYS ]; then
				print_all_details "ALL" | more
				print_iscsi_details "ALL" | more
			else
				QL_FS=$QL_PROC
				print_proc_all_details "ALL" | more
			fi
			exit 0
		else
			echo "Please select proper option"
			echo "# $QL_SCRIPT -v <--all/HOST number>"
			exit 1
		fi
		;;

		-s | --statistics )
		if [ $QL_FS == $QL_PROC ]; then
			echo "NOTE: Statistics are only available in \"sysfs\" based scanning."
			echo "Continuing with \"sysfs\" based scan..."
			QL_FS=$QL_SYS
		fi
		
		if [ $# -ne 2 ]; then
			echo "Please select proper option"
			echo "# $QL_SCRIPT  --statistics < --all/HOST number >"
			exit 1
		fi
		
		echo $2 | grep "^[0-9]" &> /dev/null
		if [ $? == 0 ]; then
                        echo ${QL_HOST[@]} | grep -w $2 &> /dev/null
                        if [ $? -ne 0 ]; then
                                echo ${QL_ISCSI_HOST[@]} |\
				 grep -w $2 &> /dev/null
                                if [ $? -ne 0 ]; then
                                        echo "ERROR: HOST $2 not found on the\
					system"
                                        exit 1
                                else
                                        iscsi_flag=1
                                fi
                        fi

                        if [ $iscsi_flag -eq 1 ] ; then
                                print_iscsi_statistics $2 | more
                        else
				print_statistics $2 | more
			fi
			exit 0
		fi

		if [ $2 == "-a" ] || [ $2 == "--all" ]; then
			print_statistics "${QL_HOST[@]}" | more
                        print_iscsi_statistics "${QL_ISCSI_HOST[@]}" | more
			exit 0
		else
			echo "Please select proper option"
			echo "# $QL_SCRIPT --statistics < --all/HOST number >"
			exit 1
		fi
		;;

		* ) 
		echo "Please select correct option"
		echo "For help run  #$0 --help"
		exit 1
		;;
	esac
	fi

	if [ $QL_PRINT_LIST == 1 ]; then
		print_all_host
		print_iscsi_host
		exit 0
	fi
	
	if [ $QL_PRINT_ALL_DETAILS -eq 1 ]; then
		if [ $QL_FS == $QL_SYS ]; then		
			print_all_details "ALL" | more
			print_iscsi_details "ALL" | more
		else
			print_proc_all_details "ALL" | more
			
		fi
		exit 0
	fi
	#HOST_QTY=$?
	#PORT_NO=$(( $HOST_QTY - 1 ))
	for HOST in ${QL_HOST[@]}
	do
		print_host_headline  #$(( $HOST_QTY - $PORT_NO ))
#			(( PORT_NO -= 1 ))
		common_display
		if [ $QL_FS == $QL_SYS ]; then
			get_targets $HOST
		else
			get_proc_targets $HOST
		fi
		if [ $? -ne 0 ]; then
		echo_b "Device Information"
		echo ""	
		fi
			
		for TARGETS in ${QL_TARGETS[@]} 
		do
			print_target_info $HOST $TARGETS 
		done  
		
		echo ""
	done | more

ISCSI_HOST=""

	for ISCSI_HOST in ${QL_ISCSI_HOST[@]}
        do
                print_iscsi_host_headline  #$(( $HOST_QTY - $PORT_NO ))
#                       (( PORT_NO -= 1 ))
                common_iscsi_display
                if [ $QL_FS == $QL_SYS ]; then
                        get_iscsi_targets $ISCSI_HOST
                else
                        get_proc_targets $HOST
                fi
                if [ $? -ne 0 ]; then
                echo_b "Device Information"
		echo ""
                fi

                for TARGETS in ${QL_ISCSI_TARGETS[@]}
                do
                        print_iscsi_target_info $ISCSI_HOST $TARGETS
                done

                echo ""
        done | more
	exit 0


