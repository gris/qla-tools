**********************************
  QLogic  HBA Snapshot Utility
**********************************
Ver: 1.16	SR Aug 24 2011
	-  Correct display message for HBA not found

Ver: 1.15	JS Aug 31 2010
        - Add iSCSI support for this Utility on sles11sp1

Ver: 1.14	SR May 12 2009
        - New README

Ver: 1.13	SR Mar 12 2009
	- Correct path for model in heading

Ver: 1.12	SR Mar 05 2009
	- Correct the syntax while checking for "if" condition.

Ver 1.11	PP Jan 15 2009
	- Added proper paths to display device and host information 
	  correctly on RHEL5 and SLES11 distros.

Ver 1.10	SR Feb 19 2008
	- Added new README
Ver 1.9		SR Aug 24 2007
	- Fix ER55978 for unwanted output.

Ver 1.8        SV/SR Apr 26 2007
	- Select different path for QLA6xxx
	- Added support for PPC64,SLES10,RHEL5

Ver 1.7        SV Feb 12 2007
        - README file name changed to README.ql-hba-snapshot.txt

Ver 1.6		SV Feb 09, 2006
	- Added check for "/proc/scsi/qla2xxx"
	- Hard coded paths are converted defines
	- Syntax check done

Ver 1.5		SR Dec 05, 2006
	- Added support for proc
	- Some new functions added & some previous functions modified
	- WWPN & WWNN added in sysfs
 
Ver 1.4		SR Oct 19, 2006
	- Added new readme

Ver 1.3		SR Oct 6, 2006
	- Renamed ql-hba-info to ql-hba-snapshot

Ver 1.2		SR June 23, 2006
	- Support for SLES10

Ver 1.1		SR June 20, 2006	
	- Modified display
	- Added Statistics options
	- Added -v/--verbose option

Ver 1.0		SR/LC June 05, 2006
	- First release


